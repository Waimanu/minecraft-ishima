# minecraft-ishima

Repository for the minecraft server Ishima.

## Server

[ishima.minecraft.waima.nu](minecraft://connect/ishima.minecraft.waima.nu)

## Mumble

[mumble.waima.nu](mumble://mumble.waima.nu)

## Mods

| Name                                                      | Version             | Discreption                          |
|:----------------------------------------------------------|:-------------------:|:-------------------------------------|
| Waystones                                                 | 9.0.1               | Teleport stones                      |
| [Balm](https://www.curseforge.com/minecraft/mc-mods/balm) | 2.3.1+0             | Library mod                          |
| Better Bundles                                            | 1.0.1               | Better Bundles                       |
| Macaw's Bridges                                           | 2.0.1               | Bridges                              |
| Project MMO                                               | 3.67.1              | Leveling and Skils plaer             |
| Sophisticated Backpacks                                   | 3.12.1.433          | Backpacks                            |
| Clumps                                                    | 8.0.3               | Bundle XP Orbs                       |
| Silent Gear                                               | 2.8.0               | Tools                                |
| Silent Lib                                                | 6.0.0               | Library mod for Silent Gear          |
| Silent's Gems                                             | 4.2.0               | Gems for Silent Gear                 |
| Advanced Chimneys                                         | 7.0.0.1-build.0043  | Decoration Chimneys                  |
| ForgeEndertech                                            | 8.0.0.0-build.0040  | Library mod for Advanced Chimneys    |
| Supplementaries                                           | 1.1.1               | Decoration stuff                     |
| Valhelsia Structures                                      | 0.1.0               | More Structures                      |
| Valhelsia Core                                            | 0.1.0               | Library mod for Valhelsia Structures |
| Filtered Chests                                           | 1.0.2               | CHest with filter                    |
| Tom's Simple Storage Mod                                  | 1.2.23              | Storage System                       |
| Awesome Dungeon - Forge                                   | 1.1.4               | More Dungeons                        |
| Library Ferret - Forge                                    | 1.0.1               | Library mod for Awesome Dungeon      |
| Storage Drawers                                           | 10.1.0              | Storage mod                          |
